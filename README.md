# Image Up & Download Application

A simple backend application to provide __*curl*__ command requests.

# GET Request:
> curl http://image-upload.jonasbohnstedt.tk:8080/upload/image.jpg > down.jpg

> Or just visit the link above

# FILE (POST) Request:
> curl -F image=@up.jpg image-upload.jonasbohnstedt.tk:8080/upload
