const express = require('express');
const fileUpload = require('express-fileupload');

const app = express();
const imgName = 'image.jpg'; // Temporally upload-image -> Will be overwritten in fs with new upload request!
const port = process.env.PORT || 8080;

app.use(fileUpload({createParentPath: true}));

// ------------------- FILE DOWNLOAD -------------------
app.use('/upload', express.static('upload'));

// ------------------- FILE UPLOAD -------------------
app.post('/upload', (req, res) => {
    if(!req.files.image) {
        res.status(400).send({error: `${imgName} property was not provided`})
        return
    }

    Object.entries(req.files).forEach(([k, f]) => f.mv('upload/' + k + '.jpg')); // load image in fs

    res.send({success: true, timestamp: new Date(), url: `image-upload.jonasbohnstedt.tk:${port}/upload/${imgName}`});
});

// Start application to listen on requests
app.listen(port, () => {
    console.log(`App listing on port ${port}`);
});